package git.doomshade.fixes.nbt;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;

public class NBT extends Utils {

	public static List<Material> MATS = new ArrayList<>();

	public NBT(Fixes plugin) {
		super(plugin);
	}

	@Override
	public void init() {
		for (String mat : getSection().getStringList("materials")) {
			if (Material.getMaterial(mat) != null)
				MATS.add(Material.getMaterial(mat));
		}
	}

	@Override
	public String getConfigSection() {
		// TODO Auto-generated method stub
		return "nbt";
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		editItem(e.getPlayer());

	}

	@EventHandler
	public void onInvClose(InventoryCloseEvent e) {
		editItem((Player) (e.getPlayer()));

	}

	private void editItem(Player player) {
		for (int i = 0; i < player.getInventory().getContents().length; i++) {
			ItemStack item = player.getInventory().getContents()[i];
			if (item == null) {
				continue;
			}

			if (!NBT.MATS.contains(item.getType())) {
				continue;
			}
			net.minecraft.server.v1_9_R1.ItemStack itemz = CraftItemStack.asNMSCopy(item);

			if (!isValid(itemz)) {
				continue;
			}
			player.getInventory().removeItem(item);
			player.getInventory().addItem(CraftItemStack.asBukkitCopy(itemz));

		}
	}

	private boolean isValid(net.minecraft.server.v1_9_R1.ItemStack itemz) {
		if (!itemz.hasTag()) {
			return false;
		}

		if (!itemz.getTag().hasKey("HideFlags")) {
			return false;
		}

		return true;
	}

}
