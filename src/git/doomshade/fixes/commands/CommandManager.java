package git.doomshade.fixes.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import git.doomshade.fixes.Fixes;

public class CommandManager implements CommandExecutor, TabCompleter {
	private final PluginCommand cmd;
	private Fixes plugin;

	public CommandManager(Fixes plugin) {
		this.plugin = plugin;
		cmd = plugin.getCommand("sinifixes");
		cmd.setExecutor(this);
	}

	public static boolean isValid(CommandSender sender, AbstractCommands acmd) {
		if ((acmd.requiresOp() && !sender.isOp()) || (acmd.requiresPlayer() && !(sender instanceof Player))) {
			return false;
		}
		return true;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			sender.sendMessage(ChatColor.DARK_AQUA + "�m-------" + ChatColor.DARK_AQUA + "[" + ChatColor.RED + "�l"
					+ plugin.getName() + ChatColor.DARK_AQUA + "]�m-------");
			String message = null;
			for (AbstractCommands acmd : AbstractCommands.CMDS) {
				if (!isValid(sender, acmd)) {
					continue;
				}
				message = infoMessage(acmd);
				if (message != null)
					sender.sendMessage(message);

			}
			return true;
		}
		for (AbstractCommands acmd : AbstractCommands.CMDS) {
			if (acmd.getCommand().equalsIgnoreCase(args[0])) {
				if (!isValid(sender, acmd)) {
					return false;
				}
				List<String> cmdArgs = acmd.getArgs() != null && acmd.getArgs().containsKey(true)
						? acmd.getArgs().get(true)
						: null;
				if (cmdArgs != null && acmd.getArgs().get(true).size() > args.length - 1) {
					sender.sendMessage(infoMessage(acmd));
					return true;
				}
				acmd.onCommand(sender, cmd, label, args);
				return true;
			}
		}

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		List<String> tab = new ArrayList<>();
		if (args.length == 0) {

			AbstractCommands.CMDS.forEach(x -> {
				if (isValid(sender, x)) {
					tab.add(x.getCommand());
				}
			});
			return tab;
		}
		for (AbstractCommands acmd : AbstractCommands.CMDS) {
			if (!isValid(sender, acmd)) {
				continue;
			}
			if (acmd.getCommand().equalsIgnoreCase(args[0])) {
				return acmd.onTabComplete(sender, cmd, label, args);
			}
			if (acmd.getCommand().startsWith(args[0])) {

				tab.add(acmd.getCommand());
			}
		}
		return tab.isEmpty() ? null : tab;
	}

	public String infoMessage(AbstractCommands acmd) {
		StringBuilder argTrue = new StringBuilder();
		StringBuilder argFalse = new StringBuilder();
		if (acmd.getArgs() != null) {
			if (acmd.getArgs().containsKey(true))
				acmd.getArgs().get(true).forEach(x -> {
					argTrue.append(" <");
					argTrue.append(x);
					argTrue.append(">");
				});
			if (acmd.getArgs().containsKey(false))
				acmd.getArgs().get(false).forEach(x -> {
					argTrue.append(" [");
					argTrue.append(x);
					argTrue.append("]");
				});
		}

		return ChatColor.DARK_AQUA + "/" + cmd.getName() + " " + acmd.getCommand() + argTrue + argFalse + ChatColor.GOLD
				+ " - " + acmd.getDescription();
	}

}
