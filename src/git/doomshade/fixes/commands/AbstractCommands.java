package git.doomshade.fixes.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import git.doomshade.fixes.Fixes;

public abstract class AbstractCommands implements CommandExecutor, TabCompleter {
	protected Fixes plugin;
	/**
	 * all commands
	 */
	public static List<AbstractCommands> CMDS = new ArrayList<>();

	/**
	 * @param plugin
	 */
	public AbstractCommands(Fixes plugin) {
		this.plugin = plugin;
		CMDS.forEach(x -> {
			if (x.getCommand().contains(getCommand())) {
				return;
			}
		});
		CMDS.add(this);
	}

	@Override
	public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

	@Override
	public abstract List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args);

	/**
	 * @return the arguments of command
	 */
	protected abstract Map<Boolean, List<String>> getArgs();

	/**
	 * @return the name of command
	 */
	public abstract String getCommand();

	/**
	 * @return the description of command
	 */
	protected abstract String getDescription();

	protected abstract boolean requiresPlayer();

	/**
	 * @return checks if the command requires op
	 */
	public abstract boolean requiresOp();
}
