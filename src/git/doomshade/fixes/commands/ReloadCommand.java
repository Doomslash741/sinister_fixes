package git.doomshade.fixes.commands;

import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import git.doomshade.fixes.Fixes;

public class ReloadCommand extends AbstractCommands {

	public ReloadCommand(Fixes plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		plugin.initFiles();
		sender.sendMessage("Plugin reloaded");
		plugin.reloadConfig();
		plugin.initUtils();
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<Boolean, List<String>> getArgs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCommand() {
		// TODO Auto-generated method stub
		return "reload";
	}

	@Override
	protected String getDescription() {
		// TODO Auto-generated method stub
		return "reloadne plugin";
	}

	@Override
	protected boolean requiresPlayer() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean requiresOp() {
		// TODO Auto-generated method stub
		return true;
	}

}
