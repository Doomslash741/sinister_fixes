package git.doomshade.fixes.prsteny;

import com.survivorserver.GlobalMarket.Market;
import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.stream.Collectors;

public class Prsteny extends Utils {


    private static final String ITEM_REGEX = "<item>";
    private static final HashSet<UUID> PROMPTS = new HashSet<>();
    private static List<Material> materials;
    private static List<Material> excluded;
    private static String postaMessage, promptMessage;
    private static List<String> restrictedWorlds = new ArrayList<>();
    private static int limit;
    private static long delay;

    private static final String[] prsteny = {"prsten", "prst�nek", "prstynek"};
    private static final String[] nahrdelniky = {"n�hrdeln�k", "nahrdelnik"};

    public Prsteny(Fixes plugin) {
        super(plugin);
    }

    @Override
    public void init() {
        materials = new ArrayList<>();
        excluded = new ArrayList<>();
        restrictedWorlds = new ArrayList<>();
        excluded.add(Material.LEVER);
        postaMessage = getSection().getString("posta-message", "&aPoslal se ti <item> &ado posty");
        promptMessage = getSection().getString("prompt-message", "&cZa 15 vterin se ti poslou veci do posty, pokud si nesklidis inv.");
        limit = getSection().getInt("limit", -1);
        delay = getSection().getInt("prompt-delay", 15) * 20;

        if (!postaMessage.isEmpty()) {
            postaMessage = ChatColor.translateAlternateColorCodes('&', postaMessage);
        }


        if (!promptMessage.isEmpty()) {
            promptMessage = ChatColor.translateAlternateColorCodes('&', promptMessage);
        }

        for (Material mat : Material.values()) {
            for (String s : Arrays.asList("door", "button", "chest", "lever"))
                if (mat.toString().toLowerCase().contains(s.toLowerCase())) {
                    excluded.add(mat);
                }
        }

        List<String> strlist = getSection().getStringList("materials");

        strlist.forEach(x -> {
            String[] split = x.split(":");
            Material mat = Material.getMaterial(split[0]);
            if (split.length == 1)
                materials.add(mat);
            else
                materials.add(new ItemStack(mat, 1, (short) Integer.parseInt(split[1])).getType());
        });

        final List<String> stringList = getSection().getStringList("restricted-worlds");
        if (stringList != null)
            restrictedWorlds.addAll(stringList.stream().map(String::toLowerCase).collect(Collectors.toList()));
        new PrstenyCommand(plugin);
    }

    @EventHandler
    public void onEat(PlayerItemConsumeEvent e) {
        ItemStack item = e.getItem();
        if (item == null) {
            return;
        }

        if (materials.contains(item.getType())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent e) {
        Block block = e.getClickedBlock();
        final Player player = e.getPlayer();
        if (!restrictedWorlds.contains(player.getWorld().getName().toLowerCase())) {
            return;
        }
        if (block != null && (excluded.contains(block.getType()) || block.getState() instanceof InventoryHolder)) {
            return;
        }
        final Action action = e.getAction();

        if (action == Action.RIGHT_CLICK_BLOCK) {
            PlayerInventory inventory = player.getInventory();
            for (Material mat : Arrays.asList(inventory.getItemInMainHand().getType(), inventory.getItemInOffHand().getType())) {
                if (materials.contains(mat)) {
                    e.setCancelled(true);
                    return;
                }
            }
        }
    }

    @EventHandler
    public void onWolfClick(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Wolf) {

            // e.setCancelled(true);
            e.setCancelled(!((Wolf) e.getRightClicked()).isAngry());
        }
    }


    private void prompt(HumanEntity holder) {

        // promptujeme s runnablem a message, pokud se player prida do hashsetu

        if (PROMPTS.add(holder.getUniqueId())) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    validate(holder, true);
                    PROMPTS.remove(holder.getUniqueId());
                }
            }.runTaskLater(plugin, delay);
            holder.sendMessage(promptMessage);
        }

    }

    private void store(HumanEntity holder, int slot) {

        PlayerInventory inv = holder.getInventory();
        ItemStack item = inv.getItem(slot);
        if (item == null) {
            return;
        }
        // display jmeno itemu, pokud ma, jinak material
        final String itemName = item.hasItemMeta() && item.getItemMeta().hasDisplayName() ? item.getItemMeta().getDisplayName() : item.getType().name();
        holder.sendMessage(postaMessage.replaceAll(ITEM_REGEX, itemName));

        String playerName = holder.getName();
        Market.getMarket().getStorage().storeMail(item, playerName, false);
        inv.setItem(slot, null);

    }

    private void validate(HumanEntity player, boolean store) {
        PlayerInventory inv = player.getInventory();

        validate(inv, 9, store, prsteny);
        validate(inv, 10, store, prsteny);
        validate(inv, 11, store, nahrdelniky);

    }

    private void validate(PlayerInventory inv, int slot, boolean store, String... names) {
        final ItemStack item = inv.getItem(slot);
        if (item == null || !item.hasItemMeta()
                || !item.getItemMeta().hasDisplayName()) {
            return;
        }
        String name = ChatColor.stripColor(item.getItemMeta().getDisplayName());
        for (String s : names) {
            if (name.toLowerCase().contains(s.toLowerCase())) {
                return;
            }

        }
        int newSlot = inv.firstEmpty();
        final HumanEntity holder = inv.getHolder();

        if (newSlot == -1) {
            if (store) {
                store(holder, slot);
            } else {
                prompt(holder);
            }
        } else {
            while ((newSlot >= 9 && newSlot <= 11) || inv.getItem(newSlot) != null) {
                newSlot++;
            }
            if (newSlot < 36) {
                inv.setItem(newSlot, item);
                inv.setItem(slot, null);
            } else {
                if (store) {
                    store(holder, slot);
                } else {
                    prompt(holder);
                }

                    /* FIX - TODO list
                    Inventory pinvv = Bukkit.createInventory(null, 9, playerName);
                    pinvv.addItem(item);
                    holder.openInventory(pinvv);
                     */
            }
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent e) {
        validate(e.getPlayer(), false);
    }

    @Override
    public String getConfigSection() {
        return "prsteny";
    }
}
