package git.doomshade.fixes.dungeonsigns;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class DungeonSign {
	private String dungeonName;
	private String command;

	public DungeonSign(String dungeonName, String command) {
		this.dungeonName = dungeonName;
		this.command = command.toLowerCase();
	}

	public final String getDungeonName() {
		return dungeonName;
	}

	public void execute(Player hrac) {
		World world = hrac.getWorld();
		String finalCmd = DungeonSigns.CMDS.get(command).substring(1).replaceAll("[$]world", world.getName())
				.replaceAll("[$]player", hrac.getName());
		Bukkit.getConsoleSender().sendMessage(finalCmd);
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), finalCmd);
	}
}