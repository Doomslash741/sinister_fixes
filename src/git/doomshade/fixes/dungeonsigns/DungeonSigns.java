package git.doomshade.fixes.dungeonsigns;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;

public class DungeonSigns extends Utils {

	public DungeonSigns(Fixes plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getConfigSection() {
		// TODO Auto-generated method stub
		return "dungeon-signs";
	}

	private static final List<DungeonSign> signs = new ArrayList<>();
	public static final Map<String, String> CMDS = new HashMap<>();
	private static final List<Block> BLOCKS = new ArrayList<>();
	private static final List<String> DELETE = new ArrayList<>();

	@Override
	public void init() {
		if (!Bukkit.getPluginManager().isPluginEnabled("MyCommand")) {
			Bukkit.getConsoleSender().sendMessage("Plugin MyCommand not found.");
			return;
		}

		File signsFile = new File("plugins/MyCommand/signsdatabase.yml");
		File cmdsFile = new File("plugins/MyCommand/commands.yml");

		if (!signsFile.exists() || !cmdsFile.exists()) {
			Bukkit.getConsoleSender().sendMessage("Required files not found.");
			return;
		}
		FileConfiguration signsLoader = YamlConfiguration.loadConfiguration(signsFile);

		signs.clear();

		for (String key : signsLoader.getKeys(false)) {
			ConfigurationSection section = signsLoader.getConfigurationSection(key);
			String sKey = key;

			if (section.isList("commands")) {
				String s = section.getStringList("commands").get(0);
				if (!s.isEmpty())
					signs.add(new DungeonSign(key, s));
			} else if (section.isConfigurationSection("Patro")) {
				ConfigurationSection patro = section.getConfigurationSection("Patro");
				String s = patro.getStringList("commands").get(0);
				if (!s.isEmpty()) {
					signs.add(new DungeonSign(key + ".Patro", s));
					sKey += ".Patro";
				}
			}
			if ((section.isBoolean("delete") ? section.getBoolean("delete") : true)) {
				DELETE.add(sKey);
			}
		}

		FileConfiguration cmdsLoader = YamlConfiguration.loadConfiguration(cmdsFile);

		for (String key : cmdsLoader.getKeys(false)) {
			ConfigurationSection section = cmdsLoader.getConfigurationSection(key);
			if (section.isList("runcmd"))
				CMDS.put(section.getString("command").toLowerCase(), section.getStringList("runcmd").get(0));
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRightClick(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK || !(e.getClickedBlock().getState() instanceof Sign)
				|| BLOCKS.contains(e.getClickedBlock())) {
			return;
		}
		BLOCKS.add(e.getClickedBlock());
		new BukkitRunnable() {
			Block block = e.getClickedBlock();

			@Override
			public void run() {
				// TODO Auto-generated method stub
				BLOCKS.remove(block);
			}

		}.runTaskLater(plugin, 20L * 1L);
		Sign sign = (Sign) (e.getClickedBlock().getState());

		if (sign.getLine(0).isEmpty() || sign.getLine(1).isEmpty()
				|| !ChatColor.stripColor(sign.getLine(0)).equalsIgnoreCase("[Dungeon]")) {
			return;
		}

		String dungeonName = ChatColor.stripColor(sign.getLine(1));

		if (dungeonName.isEmpty()) {
			return;
		}

		if (DELETE.contains(dungeonName))
			sign.getBlock().setType(Material.AIR);

		for (DungeonSign dungeonSign : signs) {
			if (dungeonSign.getDungeonName().equalsIgnoreCase(dungeonName)) {
				dungeonSign.execute(e.getPlayer());
				break;
			}
		}
	}

	@EventHandler
	public void onSignPlace(SignChangeEvent e) {
		if (e.getLine(0).equalsIgnoreCase("[Dungeon]")) {
			e.setLine(0, ChatColor.GREEN + "[Dungeon]");
			e.getPlayer().sendMessage(ChatColor.GREEN + "Sign created succesfuly..");
		}
	}

}
