package git.doomshade.fixes.cdsc.commands;

import org.bukkit.command.CommandSender;

import git.doomshade.fixes.Fixes;
import me.stutiguias.cdsc.init.Cdsc;

public class CommandCdEnd extends AbstractCdscCommands {

	private Cdsc cdsc;

	public CommandCdEnd(Fixes plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCommand() {
		// TODO Auto-generated method stub
		return "cdend";
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		// TODO Auto-generated method stub
		cdsc = Cdsc.getPlugin(Cdsc.class);

		return new CDStopEventCommand(cdsc).onCommand(sender, args);
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return "ukonci castle defense";
	}

}
