package git.doomshade.fixes.cdsc.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import git.doomshade.fixes.Fixes;

public class CdscCommandManager implements CommandExecutor {
	private Fixes plugin;

	/**
	 * @param plugin
	 */
	public CdscCommandManager(Fixes plugin) {
		this.plugin = plugin;
		this.plugin.getCommand("sinifix").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			showHelp(sender);
			return true;
		}
		for (AbstractCdscCommands cmds : AbstractCdscCommands.cmds) {
			if (cmds.getCommand().toString().equalsIgnoreCase(args[0])) {
				cmds.onCommand(sender, args);
				return true;
			}
		}
		return false;
	}

	private static void showHelp(CommandSender sender) {
		for (AbstractCdscCommands cmd : AbstractCdscCommands.cmds) {
			showHelp(sender, cmd);
		}
	}

	private static void showHelp(CommandSender sender, AbstractCdscCommands cmd) {
		if (!cmd.getCommand().equalsIgnoreCase("sinifix")) {
			sender.sendMessage(ChatColor.DARK_AQUA + cmd.toString() + ChatColor.GOLD + " - " + cmd.description());
		}

	}

}
