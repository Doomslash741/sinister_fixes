package git.doomshade.fixes.cdsc.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import git.doomshade.fixes.Fixes;

public abstract class AbstractCdscCommands {
	protected Fixes plugin;
	public static List<AbstractCdscCommands> cmds;
	static {
		cmds = new ArrayList<>();
	}

	public AbstractCdscCommands(Fixes plugin) {
		this.plugin = plugin;
		cmds.add(this);
	}

	/**
	 * @return
	 */
	public abstract String getCommand();

	public abstract String description();

	public abstract boolean onCommand(CommandSender sender, String[] args);

	@Override
	public String toString() {
		return "/sinifix " + getCommand();
	}
}
