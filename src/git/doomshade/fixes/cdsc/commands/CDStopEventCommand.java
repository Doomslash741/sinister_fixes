package git.doomshade.fixes.cdsc.commands;

import org.bukkit.command.CommandSender;

import me.stutiguias.cdsc.commands.StopEventCommand;
import me.stutiguias.cdsc.init.Cdsc;

public class CDStopEventCommand extends StopEventCommand {

	public CDStopEventCommand(Cdsc plugin) {
		super(plugin);
	}

	public boolean onCommand(CommandSender sender, String[] args) {
		return OnCommand(sender, args);
	}
}
