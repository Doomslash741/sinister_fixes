package git.doomshade.fixes.cdsc.enums;

public enum Commands {
	DEFAULT("sini"), CD_END("cdend");

	private String s;

	private Commands(String s) {
		this.s = s;
	}

	@Override
	public String toString() {
		return s;
	}
}
