package git.doomshade.fixes.cdsc;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;
import git.doomshade.fixes.cdsc.commands.CdscCommandManager;
import git.doomshade.fixes.cdsc.commands.CommandCdEnd;

public class Cdsc extends Utils {

	public Cdsc(Fixes plugin) {
		super(plugin);
	}

	@Override
	public void init() {
		new CommandCdEnd(plugin);
		new CdscCommandManager(plugin);

	}

	@Override
	public String getConfigSection() {
		// TODO Auto-generated method stub
		return null;
	}
}
