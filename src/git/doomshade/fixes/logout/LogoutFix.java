package git.doomshade.fixes.logout;

import java.io.File;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;

public class LogoutFix extends Utils {
	private static Location ydraiLoc;
	private static final Pattern INSTANCE_PATTERN = Pattern.compile("[\\w]+[-][0-9]+");

	public LogoutFix(Fixes plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		File ydraiWarpFile = new File("plugins/Essentials/warps/ydraid.yml");
		if (!ydraiWarpFile.exists()) {
			sendMessage(ChatColor.RED + "Soubor " + ydraiWarpFile.getAbsolutePath() + " neexistuje!");
			return;
		}
		FileConfiguration loader = YamlConfiguration.loadConfiguration(ydraiWarpFile);
		ydraiLoc = new Location(Bukkit.getWorld(loader.getString("world")), loader.getDouble("x"),
				loader.getDouble("y"), loader.getDouble("z"), (float) loader.getDouble("yaw"),
				(float) loader.getDouble("pitch"));
	}

	@Override
	public String getConfigSection() {
		// TODO Auto-generated method stub
		return null;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onLeave(PlayerJoinEvent e) {
		Player hrac = e.getPlayer();
		World svet = hrac.getWorld();
		if (!isInstance(svet) || ydraiLoc == null) {
			return;
		}
		hrac.teleport(ydraiLoc);
	}

	public static boolean isInstance(World svet) {
		return INSTANCE_PATTERN.matcher(svet.getName()).find();
	}

}
