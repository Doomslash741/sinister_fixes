package git.doomshade.fixes.fly;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;

import java.util.ArrayList;
import java.util.List;

public class Fly extends Utils {

	private List<String> permitted = new ArrayList<>();

	public Fly(Fixes plugin) {
		super(plugin);
	}

	@Override
	public void init() {
		permitted = getSection().getStringList("permitted");

	}

	@Override
	public String getConfigSection() {
		return "fly";
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent e) {
		Player hrac = e.getPlayer();
		GameMode gm = hrac.getGameMode();
		if (hrac.isOp() || gm == GameMode.CREATIVE) {
			return;
		}
		if (gm == GameMode.ADVENTURE || gm == GameMode.SURVIVAL) {
			hrac.setFlying(false);
			return;
		}
		if (e.getFrom().getName().equalsIgnoreCase("build")) {
			hrac.setFlying(false);
			return;
		}
		if (hrac.getWorld().getName().equalsIgnoreCase("build")) {
			return;
		}
		if (permitted.contains(hrac.getName())) {
			return;
		}
		hrac.setFlying(false);
	}

}
