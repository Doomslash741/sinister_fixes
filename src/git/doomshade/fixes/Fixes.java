package git.doomshade.fixes;

import org.bukkit.plugin.java.JavaPlugin;

import git.doomshade.fixes.cdsc.Cdsc;
import git.doomshade.fixes.commands.CommandManager;
import git.doomshade.fixes.commands.ReloadCommand;
import git.doomshade.fixes.dungeonsigns.DungeonSigns;
import git.doomshade.fixes.fly.Fly;
import git.doomshade.fixes.gamemode.Gamemode;
import git.doomshade.fixes.logout.LogoutFix;
import git.doomshade.fixes.nbt.NBT;
import git.doomshade.fixes.prsteny.Prsteny;
import git.doomshade.fixes.punisher.Punisher;
import git.doomshade.fixes.serverstats.ServerStats;
import git.doomshade.fixes.staff.StavesManager;

public class Fixes extends JavaPlugin {

	@Override
	public void onEnable() {

		// DONE
		new Cdsc(this);
		new NBT(this);
		new Fly(this);
		new Gamemode(this);
		new Prsteny(this);
		new LogoutFix(this);
		new StavesManager(this);
		new DungeonSigns(this);

		// UNDONE
		new Punisher(this);
		new ServerStats(this);

		initFiles();
		initCommands();

		// Musi byt na konci
		initUtils();
		new CommandManager(this);
	}

	public void initUtils() {
		Utils.utils.forEach((x, y) -> y.init());
	}

	public void initFiles() {
		if (!getDataFolder().isDirectory()) {
			getDataFolder().mkdir();
		}
		saveDefaultConfig();
	}

	private void initCommands() {
		new ReloadCommand(this);
	}
}
