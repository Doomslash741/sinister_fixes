package git.doomshade.fixes.staff;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import git.doomshade.fixes.Fixes;
import net.md_5.bungee.api.ChatColor;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Staff {
	private ItemStack item;
	private double cooldown;
	private String command;
	private static Map<UUID, Double> COOLDOWNS = new HashMap<>();
	private Fixes plugin = Fixes.getPlugin(Fixes.class);

	/**
	 * @param item
	 * @param cooldown
	 * @param command
	 */
	public Staff(ItemStack item, double cooldown, String command) {
		this.item = item;
		this.cooldown = cooldown;
		this.command = command;
	}

	public void cast(Player hrac) {
		if (COOLDOWNS.containsKey(hrac.getUniqueId()) && COOLDOWNS.get(hrac.getUniqueId()) <= 0) {
			COOLDOWNS.remove(hrac.getUniqueId());
		}
		if (!COOLDOWNS.containsKey(hrac.getUniqueId())) {
			PermissionUser user = PermissionsEx.getUser(hrac);
			String perm = "magicspells.cast.*";
			if (!user.has(perm)) {
				user.addPermission(perm);
				hrac.performCommand(command);
				user.removePermission(perm);
			} else {
				hrac.performCommand(command);
			}
			COOLDOWNS.put(hrac.getUniqueId(), cooldown);
			new BukkitRunnable() {

				@Override
				public void run() {
					if (COOLDOWNS.get(hrac.getUniqueId()) <= 0) {
						COOLDOWNS.remove(hrac.getUniqueId());
						cancel();
					}
					double cd = COOLDOWNS.containsKey(hrac.getUniqueId()) ? COOLDOWNS.get(hrac.getUniqueId()) : 0;
					COOLDOWNS.put(hrac.getUniqueId(), cd - 1d / 20d);
				}

			}.runTaskTimer(plugin, 0, 1L);
		}
		if (!StavesManager.RIGHTCLICK_COOLDOWNS.contains(hrac.getUniqueId())) {
			hrac.sendMessage(getFormattedCooldownMessage(COOLDOWNS.get(hrac.getUniqueId())));
		}

	}

	public String getFormattedCooldownMessage() {
		return getFormattedCooldownMessage(cooldown);
	}

	public String getFormattedCooldownMessage(double cooldown) {
		return ChatColor.translateAlternateColorCodes('&', new StavesManager(plugin).getCooldownMessage()
				.replaceAll("<cooldown>", String.valueOf((int) cooldown)));
	}

	public static Staff fromItem(ItemStack item) {
		return item != null ? fromDisplayName(item.getItemMeta().getDisplayName()) : null;
	}

	public static Staff fromDisplayName(String name) {
		return StavesManager.STAVES_NAMES.getOrDefault(ChatColor.stripColor(name), null);
	}

	@Override
	public String toString() {
		if (item != null && item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
			return item.getItemMeta().getDisplayName();
		}
		return "";
	}

	public ItemStack getItem() {
		return item;
	}
}
