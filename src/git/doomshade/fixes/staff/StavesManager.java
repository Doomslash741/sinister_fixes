package git.doomshade.fixes.staff;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;

public class StavesManager extends Utils {
    public static List<Staff> STAVES;
    public static Map<String, Staff> STAVES_NAMES;
    private static File ITEMS_FILE = new File("plugins/DiabloLike/items.yml");
	private static final Pattern CD_PATTERN = Pattern.compile("Cooldown: [0-9]+");
    public static List<UUID> RIGHTCLICK_COOLDOWNS = new ArrayList<>();

    public StavesManager(Fixes plugin) {
        super(plugin);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void init() {
        // TODO Auto-generated method stub
        ITEMS_FILE = new File("plugins/DiabloLike/items.yml");
		File[] COLLECTION_FILES = new File("plugins/DiabloLike/collections").listFiles();
		File[] MOB_SPECIFIC_FILES = new File("plugins/DiabloLike/mobspecific").listFiles();
        STAVES = new ArrayList<>();
        STAVES_NAMES = new HashMap<>();
        List<FileConfiguration> loaders = new ArrayList<>();

        if (ITEMS_FILE.exists())
            loaders.add(YamlConfiguration.loadConfiguration(ITEMS_FILE));

		if (COLLECTION_FILES != null && COLLECTION_FILES.length != 0) for (File file : COLLECTION_FILES) {
			loaders.add(YamlConfiguration.loadConfiguration(file));
		}

		if (MOB_SPECIFIC_FILES != null && MOB_SPECIFIC_FILES.length != 0) for (File file : MOB_SPECIFIC_FILES) {
			loaders.add(YamlConfiguration.loadConfiguration(file));
		}
		for (FileConfiguration loader : loaders)
            for (String key : loader.getKeys(false)) {
                if (!loader.isConfigurationSection(key)) {
                    continue;
                }
                ConfigurationSection section = loader.getConfigurationSection(key);
                if (!section.contains("cast")) {
                    continue;
                }

                ItemStack item = getItemFromFile(key, loader);
                if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasLore()) {
                    continue;
                }
                double cooldown = 5;

                for (String s : item.getItemMeta().getLore()) {
                    String strip = ChatColor.stripColor(s);
                    if (CD_PATTERN.matcher(strip).find()) {
                        cooldown = Integer.parseInt(strip.replaceAll("[\\D]", ""));
                    }
                }
                Staff staff = new Staff(item, cooldown, "cast " + section.getString("cast"));
                STAVES.add(staff);
                try {
                    STAVES_NAMES.put(ChatColor.stripColor(staff.getItem().getItemMeta().getDisplayName()), staff);
                } catch (Exception e) {
                }

            }
        //
        // DEBUG
        sendMessage(ChatColor.GREEN + "Loaded " + STAVES.size() + " staves.");
    }

    @Override
    public String getConfigSection() {
        // TODO Auto-generated method stub
        return "staves";
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent e) {
        HumanEntity entity = e.getPlayer();
        if (!(entity instanceof Player)) {
            return;
        }
        Player hrac = (Player) entity;
        updatePlayerInv(hrac);
    }

    @EventHandler
    public void onSwap(PlayerSwapHandItemsEvent e) {
        ItemStack oh = e.getOffHandItem();
        if (oh == null || !oh.hasItemMeta() || !oh.getItemMeta().hasDisplayName()) {
            return;
        }
        e.setCancelled(Staff.fromItem(oh) != null);

    }

    private void updatePlayerInv(Player hrac) {
        if (!isValid(hrac.getInventory())) {
            return;
        }
        PlayerInventory inv = hrac.getInventory();
        ItemStack possibleStaff = inv.getItemInOffHand();
        Staff staff = Staff.fromItem(possibleStaff);
        if (staff == null) {
            return;
        }

        if (inv.firstEmpty() != -1) {
            inv.setItemInOffHand(null);
            inv.addItem(possibleStaff);
        }

        hrac.sendMessage(ChatColor.RED + "Nem��e� nosit h�l v lev� ruce.");
    }

    private boolean isValid(PlayerInventory inv) {
        return !(inv == null || inv.getItemInOffHand() == null || !inv.getItemInOffHand().hasItemMeta()
                || !inv.getItemInOffHand().getItemMeta().hasDisplayName());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCast(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player hrac = e.getPlayer();
            ItemStack item = hrac.getInventory().getItemInMainHand();
            if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName()) {
                return;
            }
            Staff staff = Staff.fromItem(item);
            if (staff == null) {
                return;
            }
            staff.cast(hrac);
            if (!RIGHTCLICK_COOLDOWNS.contains(hrac.getUniqueId())) {
                RIGHTCLICK_COOLDOWNS.add(hrac.getUniqueId());
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        RIGHTCLICK_COOLDOWNS.remove(hrac.getUniqueId());

                    }

                }.runTaskLater(plugin, 20);
            }
        }
    }

    public String getCooldownMessage() {
        return getSection().getString("cooldown-message");
    }

    public static ItemStack getItemFromFile(String s, FileConfiguration loader) {
        if (!loader.isConfigurationSection(s)) {
            Bukkit.getConsoleSender().sendMessage(
                    String.format(ChatColor.RED + "Config jmeno %s itemu neni v %s!", s, loader.toString()));
            return null;
        }

        ConfigurationSection section = loader.getConfigurationSection(s);
        int data = section.isInt("data") ? section.getInt("data") : 0;

        try {
            return makeItem(Material.getMaterial(section.getString("materialName")), section.getString("displayName"),
                    section.getStringList("lore"), (short) data);
        } catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage(s);
            return null;
        }
    }

    @SuppressWarnings("unused")
    private static ItemStack getItemFromFile(String s) {
        return getItemFromFile(s, YamlConfiguration.loadConfiguration(ITEMS_FILE));
    }

    private static ItemStack makeItem(Material mat, String displayName, List<String> lore, short data) {
        return makeItem(mat, displayName, lore, data, Color.AQUA);
    }

    private static ItemStack makeItem(Material mat, String displayName, List<String> lore, short data, Color color) {
        ItemStack item = new ItemStack(mat, 1, data);
        if (isLeather(mat)) {
            LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
            meta.setLore(translateLore(lore));
            meta.setColor(color);
            item.setItemMeta(meta);
        } else {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
            meta.setLore(translateLore(lore));
            item.setItemMeta(meta);
        }
        return item;
    }

    private static List<String> translateLore(List<String> lore) {
        List<String> newLore = new ArrayList<String>();
        lore.forEach(s -> newLore.add(ChatColor.translateAlternateColorCodes('&', s)));
        return newLore;
    }

    private static boolean isLeather(Material mat) {
        switch (mat) {
            case LEATHER_HELMET:
            case LEATHER_CHESTPLATE:
            case LEATHER_LEGGINGS:
            case LEATHER_BOOTS:
                return true;
            default:
                return false;
        }
    }
}
