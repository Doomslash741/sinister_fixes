package git.doomshade.fixes;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Listener;

public abstract class Utils implements Listener {
	protected Fixes plugin;
	public static HashMap<String, Utils> utils = new HashMap<>();
	private final String prefix;
	private final ConfigurationSection section;

	public Utils(Fixes plugin) {
		this.plugin = plugin;
		this.prefix = String.format("[%s] ", plugin.getName());
		this.section = getConfigSection() == null ? null : plugin.getConfig().getConfigurationSection(getConfigSection());
		if (!utils.containsKey(getClass().getName())) {
			utils.put(getClass().getName(), this);
			Bukkit.getPluginManager().registerEvents(this, plugin);
		}
	}

	public abstract void init();

	public abstract String getConfigSection();

	protected ConfigurationSection getSection() {
		return section;
	}
	protected void sendMessage(String message) {
		Bukkit.getConsoleSender().sendMessage(prefix + message);
	}
}
