package git.doomshade.fixes.gamemode;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import git.doomshade.fixes.Fixes;
import git.doomshade.fixes.Utils;

public class Gamemode extends Utils {

	public Gamemode(Fixes plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() {
	}

	@Override
	public String getConfigSection() {
		// TODO Auto-generated method stub
		return null;
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent e) {
		Player hrac = e.getPlayer();
		if (hrac.isOp()) {
			return;
		}
		if (hrac.getGameMode().equals(GameMode.CREATIVE)) {
			return;
		}

		if (hrac.getWorld().getName().equalsIgnoreCase("build")) {
			hrac.setGameMode(GameMode.SURVIVAL);
			return;
		}
		hrac.setGameMode(GameMode.ADVENTURE);
	}

}
